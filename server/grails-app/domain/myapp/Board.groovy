package myapp

import grails.rest.Resource


@Resource(uri='/board')
class Board {

    String name;
    String title;
    String content;


    static constraints = {
    }
}
