import React, {Component} from 'react';
import {Col, Row} from 'reactstrap'
import {CLIENT_VERSION, REACT_VERSION, SERVER_URL} from './config';
import 'whatwg-fetch';
import Footer from "./Footer";
import Button from '@material-ui/core/Button';
import Box from "@material-ui/core/Box";
import axios from 'axios';
import CircularProgress from "@material-ui/core/CircularProgress";
import ADButton from 'antd/es/button';
import Flexbox from 'flexbox-react';
import './css/App.css'

export default class App extends Component {


    constructor() {
        super()
        this.state = {
            serverInfo: {},
            clientInfo: {
                version: CLIENT_VERSION,
                react: REACT_VERSION
            },
            collapse: false,
            results: [],
            dummy: 'lksdflksd',
            loading: false,
        }
    }

    toggle = () => {
        this.setState({collapse: !!this.state.collapse})
    }

    async componentDidMount() {

        await this.getList()

    }

    async getList() {
        let results = await fetch('http://localhost:8080/Board').then(r => r.json()).then(response => {
            console.log('debug', response);

            return response;

        })

        this.setState({
            results: results,
        })
    }

    async deleteData(item) {
        const response = await axios.delete(
            'http://localhost:8080/Board/' + item.id,
            {},
            {headers: {'Content-Type': 'application/json'}}
        )
        console.log('debug', response.status);

        if (response.status === 204) {
            this.getList();

        }
    }

    async insertData() {
        const response = await axios.post(
            'http://localhost:8080/Board',
            {
                title: 'data',
                name: 'nicejbjb_genius',
                content: 'sflkdslfkdls_content',
            },
            {headers: {'Content-Type': 'application/json'}}
        )
        console.log(response.data)
        await this.getList();
    }

    renderAntdButton() {
        return (

            <div>
                <Flexbox flexDirection="row" style={{height:50, }}>
                    <Flexbox style={{flex:.3, backgroundColor:'white', margin:5, alignItems:'center', justifyContent:'center'}}>
                        <ADButton type="primary">Button</ADButton>
                    </Flexbox>

                    <Flexbox style={{flex:.3, backgroundColor:'grey', margin:5, alignItems:'center', justifyContent:'center'}}>
                        <ADButton type="dashed" size={10}>고경준 천재님이십니디ㅏsdlfksdlfk</ADButton>
                    </Flexbox>

                    <Flexbox style={{flex:.3, backgroundColor:'grey', margin:5, alignItems:'center', justifyContent:'center'}}>
                        <ADButton type="dashed" size={10}>고경준 천재님이십니디ㅏsdlfksdlfk</ADButton>
                    </Flexbox>
                </Flexbox>
                <Flexbox style={{height:50}} flexDirection="column">
                    <div className='container'>sldkflsdkf</div>
                    <div className='container'>sldkflsdkf</div>
                    <div className='container'>sldkflsdkf</div>
                </Flexbox>
            </div>

        );
    }

    render() {
        const {serverInfo, clientInfo, collapse} = this.state;

        return [


            <Col key={2}>

                {this.state.results.map(item => {

                    return (
                        <Row>
                            <Box style={{margin: 5}}>{item.name}</Box>
                            <Box style={{margin: 5}}>
                                <Button variant="contained" onClick={async () => {
                                    await this.deleteData(item);
                                }}>
                                    delete
                                </Button>
                            </Box>
                        </Row>
                    )
                })}

            </Col>,
            <Box style={{height: 50,marginBottom:50}}>

                {this.renderAntdButton()}
            </Box>,
            <div>
                <Col style={{flexDirection: 'row'}}>
                    <Box>
                        <Button
                            variant="contained"
                            style={{backgroundColor: 'red', color: 'white'}}
                            onClick={async () => {

                                await this.insertData()

                            }}
                        >
                            Commit data
                        </Button>
                    </Box>
                    <Box style={{height: 25}}/>
                    <Box>
                        <Button variant="contained">
                            Default
                        </Button>
                    </Box>
                    <Box style={{height: 25}}/>
                    <Box>
                        <Button variant="contained">
                            Default
                        </Button>
                    </Box>
                    <Box style={{height: 25}}/>
                    <Box>
                        <Button variant="contained" style={{backgroundColor: 'blue', color: 'white'}} onClick={() => {
                            this.setState({
                                loading: !this.state.loading
                            })
                        }}>
                            Toggle Laoding
                        </Button>
                    </Box>
                </Col>
                {this.state.loading && <Box style={{margin: 30}}>
                    <CircularProgress color="secondary"/>
                </Box>}

            </div>,


        ];
    }
}


